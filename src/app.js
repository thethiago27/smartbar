const express = require('express');
const app = express();
const path = require('path');

const http = require('http').createServer(app);
const cookieParser = require('cookie-parser')

app.use(express.static(path.join(__dirname, 'views/assets/libs')));

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(cookieParser());


app.get('/', (req, res, next) => {

    res.render('pages/index', {
        title: 'Início'
    });
    next();
});

app.get('/mesas', (req, res) => {

    res.render('pages/mesas', {
        title: 'Mesas Disponíveis'
    });

});

let products;

app.get('/produtos', (req, res) => {

    res.render('pages/produtos', {
        title: 'Produtos'
    });
});

app.get('/produtos/add/', (req, res) => {

    res.render('pages/add', {
        title: 'Adicionar novos produtos'
    });
});

app.get('/lucros', (req, res) => {

    res.render('pages/lucros', {
        title: 'Lucros'
    });

});

app.get('/update', (req, res) => {

    res.render('pages/update', {
        title: 'Boas Novas!'
    });

});

http.listen(8700, () => {
    console.log('listening on *:8700');
});

