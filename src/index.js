const {app, BrowserWindow, autoUpdater, ipcMain, dialog} = require('electron');
const path = require('path');

require('./app.js');
require('./server.js')

if (require('electron-squirrel-startup')) {
    app.quit();
}

const server = 'https://sb-sup.herokuapp.com';
const url = `${server}/download/${process.platform}`;

autoUpdater.setFeedURL({url});
// setInterval(() => {
//     autoUpdater.checkForUpdates();
// }, 60000);

const createWindow = () => {
    const mainWindow = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true
        }
    });

    mainWindow.loadURL(`http://localhost:8700/`);


    autoUpdater.on('update-available', () => {

        ipcMain.on('update-available', (event, arg) => {
            console.log("Update is available!");
            event.returnValue = 'update!';
        });

        autoUpdater.downloadUpdate().then((path) => {
            console.log('download path', path);
        }).catch((e) => {
            console.log(e);
        })
    });

    autoUpdater.once('error', (err, ev) => {
        ipcMain.on('error', (event, arg) => {
            event.returnValue = 'update!';
        });
        console.log("Update is error!" + err);

        const dialogOpts = {
            type: 'info',
            buttons: ['Restart', 'Later'],
            title: 'Application Update',
            detail: 'A new version has been downloaded. Restart the application to apply the updates.'
        }

        dialog.showMessageBox(dialogOpts).then((returnValue) => {

        });
    });

    autoUpdater.on('update-not-available', () => {
        ipcMain.on('not_update', (event, arg) => {
            console.error("Update is error!");
            event.returnValue = 'update!';
        });

    });

    autoUpdater.on('update-downloaded', (event, releaseNotes, releaseName) => {
        const dialogOpts = {
            type: 'info',
            buttons: ['Restart', 'Later'],
            title: 'Application Update',
            detail: 'A new version has been downloaded. Restart the application to apply the updates.'
        }

        dialog.showMessageBox(dialogOpts).then((returnValue) => {
            if (returnValue.response === 0) autoUpdater.quitAndInstall()
        });
    })

}


app.on('ready', createWindow);


app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {

    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
});

