const {shell} = require('electron');

Vue.component('navbar', {

    template: '<ul class="navbar">' +
        '<li style="display: " id="bar-btn" onclick="openSideBar()" class="navbar-link"><a href="#news"><i class="fas fa-bars"></i></a></li>' +
        '<li style="display: none"  id="close-btn" onclick="closeSideBar()" class="navbar-link"><a href="#news"><i class="fas fa-times"></i></a></li>' +
        '<li class="navbar-link"><a onclick="openLink()" href="#">SmartBar</a></li>' +

        '</ul>'
});

new Vue({el: '#navbar-components'});

const openSideBar = () => {

    document.getElementById('bar-btn').style.display = "none";

    document.getElementById('close-btn').style.display = "block";
    document.getElementById("mySidenav").style.display = "block";
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

const closeSideBar = () => {

    document.getElementById('bar-btn').style.display = "block";

    document.getElementById('close-btn').style.display = "none";

    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
}

const openLink = () => {
    shell.openExternal('https://smartbar.net/').then(r => console.log("test"));
}

