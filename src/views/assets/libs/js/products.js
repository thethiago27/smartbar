const axios = require('axios');
const express = require('express');

new Vue({
    el: '#app',
    data() {
        return {
            info: null,
            loading: true,
            error: false,
            msg_error: "",
            empty: false
        }
    },
    methods: {
        deleteProduct: (id) => {
            axios.delete('http://localhost:8710/produtos/' + id).then((resp) => {
                window.location.href = "/produtos";
            }).catch((err) => {
                alert(err);
            });
        }
    },
    mounted() {
        axios.get('http://localhost:8710/produtos/')
            .then(response => {

                if (response.data > -1) {
                    this.empty = true;
                    return;
                }

                this.info = response.data
            }).catch(error => {
            console.log(error)
            this.error = true
        }).finally(() => this.loading = false)
    }

})

const search = () => {

    let input, filter, div, span, result, empty, txtValue, i;

    input = document.getElementById('search');

    filter = input.value.toUpperCase();

    div = document.getElementById('result');
    span = div.getElementsByTagName('div');

    empty = document.getElementById('empty');

    for (i = 0; i < span.length; i++) {
        result = span[i].getElementsByTagName("span")[0];

        if (result) {
            txtValue = result.textContent || result.innerText;

            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                span[i].style.display = "";
                empty.style.display = "none";
            } else {
                span[i].style.display = "none";
                empty.style.display = "block";
            }
        }

    }
}

const addProduct = (product_name, price) => {

    return new Promise((resolve, reject) => {

        axios.post('http://localhost:8710/produtos/', {
            product_name: product_name,
            price: price
        }).then((resp) => {

            resolve(resp);

        }).catch((err) => {

            reject('ERROR');

        });
    });


}
